const mongoose = require(`mongoose`)

const ShipmentSchema = new mongoose.Schema({
  name: String,
  code: String,
  easyPostId: String
})

const Shipment = mongoose.model(`Shipment`, ShipmentSchema)

module.exports = Shipment

