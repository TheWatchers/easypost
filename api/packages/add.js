const shippingCompany = require(`../_lib/shipping-company`)
const authCheck = require(`../_lib/auth`)
const Shipment = require(`../_Shipment`)
const mongoose = require(`mongoose`)
const EasyPost = require(`@easypost/api`)
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

mongoose.connect(process.env.MONGO_URL)

const easypost = new EasyPost(process.env.EASYPOST_KEY)

const handler = async (req, res) => {
  const { trackingCode, name } = req.body

  const tracker = new easypost.Tracker({
    tracking_code: trackingCode,
    carrier: shippingCompany(trackingCode)
  })

  const result = await tracker.save()

  await Shipment.create({
    name,
    easyPostId: result.id
  })

  return res.status(201).json({ id: result.id })
}

module.exports = (req, res) => {
  return authCheck(CLAIMS.easypost.add)(req, res, handler)
}
