const { promisify: pify } = require(`util`)
const authCheck = require(`../_lib/auth`)
const Shipment = require(`../_Shipment`)
const mongoose = require(`mongoose`)
const EasyPost = require(`@easypost/api`)
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

mongoose.connect(process.env.MONGO_URL)

const handler = async (req, res) => {
  try {
    const easypost = new EasyPost(process.env.EASYPOST_KEY)

    const { trackingCode } = req.query

    const tracking = await easypost.Tracker.retrieve(trackingCode)

    return res.json(tracking)
  } catch (e) {
    console.log(`error occurred looking up ${req.query.trackingCode}: `, e)

    return res.status(500).send(e)
  }
}

module.exports = (req, res) => {
  return authCheck(CLAIMS.easypost.get)(req, res, handler)
}

