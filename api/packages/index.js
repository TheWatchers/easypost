const authCheck = require(`../_lib/auth`)
const Shipment = require(`../_Shipment`)
const mongoose = require(`mongoose`)
const EasyPost = require(`@easypost/api`)
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

mongoose.connect(process.env.MONGO_URL)

const easypost = new EasyPost(process.env.EASYPOST_KEY)

const handler = async (req, res) => {
  const {
    authorization
  } = req.headers

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.easypost.add, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const { trackingCode } = req.body

  let shipments = await Shipment.find()

  shipments = shipments.reduce((list, shipment) => {
    if (!list[shipment.easyPostId]) {
      shipment.__v = undefined

      list[shipment.easyPostId] = shipment
    }

    return list
  }, {})

  return res.json(Object.values(shipments))
}

module.exports = (req, res) => {
  return authCheck(CLAIMS.easypost.get)(req, res, handler)
}

