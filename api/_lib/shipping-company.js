module.exports = (trackingCode) => {
  if (trackingCode.match(/(1Z)(\d{16})/)) return 'UPS';
  if (trackingCode.match(/^[0-9]{12,22}$/)) return 'FEDEX';
  if (trackingCode.match(/\b(94|92)(0|7)(0|5|7|8|2)(1|5|3|8)(\d{17})/) || trackingCode.match(/(EA)(\d{9})(US)/)) return 'USPS';
}

